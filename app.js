new Vue({
	el: '#app',
	data: {
		gameOn: false,
		player: {
			name: 'Chris',
			health: 100,
			stamina: 3 // number of special attacks available
		},
		monster: {
			name: 'Odyn',
			health: 100
		},
		winner: '',
		history: [] // keeps record of dmg done
	},
	methods: {
		startTheGame() {
			this.player.health = 100;
			this.monster.health = 100;
			this.winner = '';
			this.history = [];
			return this.gameOn = !this.gameOn;
		},
		// draw random number of dmg
		randomNumber(min, max) {
			return number = Math.floor(Math.random() * (max - min) + min);
		},
		playerDealsDamage(min, max) {
			let playerAttack = this.randomNumber(min, max);
			return this.updateHealth(this.monster, playerAttack, this.player);
		},
		updateHealth(person, attack, winner) {
			if( person.health - attack <= 0 ) {
				person.health = 0;
				this.gameOver(winner.name);
				return attack;
			} else {
				person.health -= attack;
				return attack;
			}
		},		
		monsterDealsDamage(min, max) {
			let monsterAttack = this.randomNumber(min, max);
			return this.updateHealth(this.player, monsterAttack, this.monster)
		},
		attack() {
			let playerAttack = this.playerDealsDamage(1, 10);
			console.log(playerAttack);
			let monsterAttack = this.monsterDealsDamage(1, 15);
			this.updateHistory(playerAttack, monsterAttack);
		},
		criticalAttack() {
			let playerAttack = '';

			// check if player has stamina to use special attack
			(this.player.stamina <= 0) ? playerAttack = this.playerDealsDamage(1, 10) : playerAttack = this.playerDealsDamage(1, 20);
			let monsterAttack = this.monsterDealsDamage(1, 15);
			this.updateHistory(playerAttack, monsterAttack);
			this.player.stamina -= 1;
		},
		heal() {
			let healing = this.randomNumber(5, 15);
			let monsterAttack = this.monsterDealsDamage(1, 15);
			this.updateHistory(healing, monsterAttack, 'healingTurn');

			if( this.player.health + healing > 100) {
				return 100;
			} else if(this.player.health + healing <= 0) {
				return 0;
			}
			else {
				return this.player.health += healing;
			}
		},
		surrender() {
			this.player.health = 100;
			this.monster.health = 100;
			this.gameOver(this.monster.name);
		},
		gameOver(winnerName) {
			this.gameOn = false;
			this.winner = winnerName;
		},
		updateHistory(playerAction, monsterAction, healingTurn) {
			this.history.unshift('Monster dealt: ' + monsterAction + ' dmg');
			if(healingTurn) {
				this.history.unshift('Player healed: ' + playerAction + ' points');
			} else {
				this.history.unshift('Player dealt: ' + playerAction + ' dmg');
			}
		}
	}
});